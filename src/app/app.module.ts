import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoggerModule} from "./modules/logger/logger.module";
import {LoggerLvl} from "./modules/logger/logger.model";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoggerModule.forRoot({colors: {debug: "#000",
        log: "#0F0",
        error: '#F00',
        warning: "#ff5100"}, logLvl: LoggerLvl.ERR})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
