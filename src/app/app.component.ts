import { Component } from '@angular/core';
import {LoggerService} from "./modules/logger/logger.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'logger-demo';

  constructor(private $log: LoggerService) {
    $log.debug(AppComponent.name, "Demo");
  }
}
