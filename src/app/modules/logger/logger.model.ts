export enum LoggerLvl {
  DEBUG,
  LOG,
  WARN,
  ERR
};

export type LoggerOptions = {
  colors: {debug: string, log: string, warning: string, error: string},
  logLvl: LoggerLvl
};
