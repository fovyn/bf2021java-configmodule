import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoggerLvl, LoggerOptions} from "./logger.model";
import {LoggerService} from "./logger.service";

const DEFAULT_OPTIONS: LoggerOptions = {
  colors: {
    debug: "#000",
    log: "#0F0",
    error: '#F00',
    warning: "#ff5100"
  },
  logLvl: LoggerLvl.DEBUG
}

export const LOG_COLORS = 'logger.colors';
export const LOG_LEVEL = 'logger.level';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class LoggerModule {
  static forRoot(opts?: LoggerOptions): ModuleWithProviders<LoggerModule> {
    const providers = [];

    const optsValue = {...DEFAULT_OPTIONS, ...opts};

    providers.push(
      { provide: LOG_COLORS, useValue: optsValue.colors },
      { provide: LOG_LEVEL, useValue: optsValue.logLvl }
    );

    return {
      ngModule: LoggerModule,
      providers: [LoggerService,...providers]
    }
  }
}
