import {Inject, Injectable} from '@angular/core';
import {LoggerLvl} from "./logger.model";
import {LOG_COLORS, LOG_LEVEL} from "./logger.module";
import {format} from 'date-fns';

@Injectable()
export class LoggerService {

  constructor(@Inject(LOG_COLORS) private $logColors: any, @Inject(LOG_LEVEL) private $logLvl: LoggerLvl) { }

  debug(src: string, msg: any): void {
    console.log(src, msg, this.$logColors, this.$logLvl, this.$logLvl <= LoggerLvl.DEBUG);
    if (this.$logLvl <= LoggerLvl.DEBUG) {
      const now = new Date();

      console.debug(`%c${format(now, 'yyyy-MM-dd')}:${src} => `, `color:${this.$logColors.debug.toLowerCase()}`, msg);
    }
  }
}
